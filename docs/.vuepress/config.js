module.exports = {
	title: '达摩猿',//你的专属网名
	description: '等风来',//你的专属网站描述
	base: '/greek/',//你的专属网站上下文，需要提交申请，申请后可用
	host: '0.0.0.0',//这个host不能动
	port: '8851', //这个端口不能动
	themeConfig: {
		nav: [
			{ text: '首页', link: '/', target: '_self'},
			{ text: '开源', 
			  items: [
			  {text:'前端',link:'/nav/font/'},
			  {text:'app',link:'/nav/app/'},
			  {text:'杂项', items:[
                  {text:'杂项一',link:'/nav/other/othera/'},
                  {text:'杂项二',link:'/nav/other/otherb/'}
			  ]
              },
			  {text:'工具', items: [
			  	{text:'git',link:'/nav/tools/git/'}, {text:'jdk',link:'/nav/tools/jdk/'}
			  	]
			  }
			  ]
			},
			{
				text:'我的', items: [
				{text:'说明',link:'/nav/my/' ,target:'_blank'},
                {text:'简介',link:'/nav/my/'}
				]
			}
		],
		sidebar: [
			['/', '首页'],
			['/page-a/', '在线.md编辑说明'],
			['/page-b/', '由感而发'],
		]
	},
	search: true,
	searchMaxSuggestions: 10,
    lastUpdated: 'Last Updated',
    "devDependencies": {
        "vuepress": "^0.14.8"
    }
}