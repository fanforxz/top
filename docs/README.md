![gezi](http://renzaijianghu.club/huozhe/static/img/logo-login.png)

## 达摩猿是什么？
达摩猿是一个个人知识分享平台。

# 好消息
达摩猿可以帮你搭建个人在线博客了！
你想拥有一个完全属于自己的在线博客吗？
你想打造一个完全属于自己的网络平台吗？
而且目前是免费的、免费的哦。

怎么操作？
```javascript
var 申请说明 = {
    "1": "首先你要通过下方的邮件或者QQ联系我，说明使用用途和部署站点的基础路径",
    "2": "审核通过后，我会分给你demo工程git地址",
    "3": "然后你就可以开始在demo工程基础上快乐的搭建属于自己的小天地了",
    "4": "本地调试完成后，push到仓库中",
    "5": "邮件或者QQ联系我发布，我会再次对你提交的代码进行审核，审核通过后，我会发布，发布完成，我会通知你线上访问url",
    "6": "ok，至此你就完成了全部流程，可以在线欣赏自己的杰作了，就这么简单！"
}

var 如何修改 = {
    "1": "拿到demo工程代码后，导入各种ide都可以",
    "2": "可以修改docs/.vuepress/config.js里的配置，以适用自己的需求，但是请不要修改host和port两个参数",
    "3": "导航可根据自己的需要进行修改",
    "4": "其他页面可根据自己的需要进行修改",
    "5": "base需要配置申请审批通过的，也就是在【申请说明】的第一点中提到的部署站点的基础路径"
}

var 注意事项 = {
    "1": "首先声明服务器是个人的，所以工程代码必须由本人管理，发布前需要审核",
    "2": "由于是个人维护的，也因为是免费的，所以发布时间由管理员（也就是本人）确定，一般情况下都会及时响应的",
    "3": "目前只接受学习型项目，不接受商业用途"
}
```
## 达摩猿是如何运作的？
达摩猿是采用VuePress构建，GitHub发布的纯静态文档类网站。

## VuePress是什么？
Vue 驱动的静态网站生成器

## GitHub是什么？
GitHub是一个面向开源及私有软件项目的托管平台，因为只支持git 作为唯一的版本库格式进行托管，故名GitHub。

## md文件是什么?
Machine Description文件 ：包括一个目标机器支持的每个指令的指令模式的开发文件，被 GNU 编译程序集（GCC）引用，通常用于Unix系统中的编译程序

## VuePress有哪些功能？

* 简洁至上：以 Markdown 为中心的项目结构，以最少的配置帮助你专注于写作。
* Vue 驱动：享受 Vue + webpack 的开发体验，可以在 Markdown 中使用 Vue 组件，又可以使用 Vue 来开发自定义主题。
* 高性能：VuePress 会为每个页面预渲染生成静态的 HTML，同时，每个页面被加载的时候，将作为 SPA 运行。

## 有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(fanforxz#163.com, 把#换成@)
* QQ: 1596400698
* 博客: [@人在江湖](http://renzaijianghu.club)
* 订阅号: [@上金荣](上金荣)

## 互联网天梯
目前各大云平台优惠多多，有想法的同学可以考虑考虑

* [【阿里云】最高￥2000云产品通用代金券。](https://www.aliyun.com/minisite/goods?userCode=3tj9udyb)
* [【阿里云感恩回馈，云产品冰点价】云服务器74元/年，这里有专享低价和代金券。](https://www.aliyun.com/minisite/goods?userCode=3tj9udyb&share_source=copy_link) 
* [【腾讯云】云产品采购季，助力行业复工。1核2G云服务器，首年99元。](https://cloud.tencent.com/act/cps/redirect?redirect=1053&cps_key=87a3dab3879a43e0a7d53507171bb99a&from=console)
* [【腾讯云】新客户无门槛领取总价值高达2860元代金券，每种代金券限量500张，先到先得。](https://cloud.tencent.com/act/cps/redirect?redirect=1040&cps_key=87a3dab3879a43e0a7d53507171bb99a&from=console)
* [【腾讯云】为初创客户提供一站式云端解决方案，快速低成本部署业务。](https://cloud.tencent.com/act/cps/redirect?redirect=1026&cps_key=87a3dab3879a43e0a7d53507171bb99a&from=console)

##关于作者

```javascript
  var ihubo = {
    nickName  : "人在江湖",
    site : "http://renzaijianghu.club"
  }
```